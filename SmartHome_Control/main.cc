/*
 * main.cc
 *
 *  Created on: Jul 28, 2015
 *      Author: messier
 */
#include <iostream>
#include <string>
#include <unistd.h>
#include "SimpleGPIO.h"
#include <vector>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include "iomap.h"

#define URL_STRING	"http://linux.zz.mu/display2.php"

struct url_data {
	size_t size;
	char* data;
};

size_t write_data(void *ptr, size_t size, size_t nmemb, struct url_data *data) {
	size_t index = data->size;
	size_t n = (size * nmemb);
	char* tmp;

	data->size += (size * nmemb);

#ifdef DEBUG
	fprintf(stderr, "data at %p size=%ld nmemb=%ld\n", ptr, size, nmemb);
#endif
	tmp = (char*)realloc(data->data, data->size + 1); /* +1 for '\0' */

	if(tmp) {
		data->data = tmp;
	} else {
		if(data->data) {
			free(data->data);
		}
		fprintf(stderr, "Failed to allocate memory.\n");
		return 0;
	}

	memcpy((data->data + index), ptr, n);
	data->data[data->size] = '\0';

	return size * nmemb;
}

char *handle_url(char* url, char *buffer, int buffer_size) {
	CURL *curl;

	struct url_data data;
	data.size = 0;
	data.data = (char*)malloc(4096); /* reasonable size initial buffer */
	if(NULL == data.data) {
		fprintf(stderr, "Failed to allocate memory.\n");
		return NULL;
	}
	data.data[0] = '\0';
	CURLcode res;
	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
		res = curl_easy_perform(curl);
		if(res != CURLE_OK) {
			fprintf(stderr, "Can't access to server\n ");
			//fprintf(stderr, "curl_easy_perform() failed: %s\n",
			//            curl_easy_strerror(res));
		}
		curl_easy_cleanup(curl);

	}
	strncpy(buffer, data.data, buffer_size);
	free(data.data);
	return buffer;
}

void initGPIO(void){
	std::vector<IOMap>::iterator it;
	for(it = ioMap.begin(); it != ioMap.end(); ++it){
		gpio_export(it->ioNum);
		gpio_set_dir(it->ioNum, OUTPUT_PIN);
		gpio_set_value(it->ioNum, LOW);
	}
}

void initIoMap(){
	for(int i = 0; i < NUM_OF_DEVICES; i++){
		ioMap.push_back(iomap_array[i]);
	}
}

bool is_gpio_valid(unsigned int gpio){
	std::vector<IOMap>::iterator it;
	for(it = ioMap.begin(); it != ioMap.end(); it++){
		if(gpio == it->ioNum) return true;
	}
	return false;
}

void testGPIO(void){
	unsigned int gpio_no, gpio_val;
	printf("Set GPIO value\n");
	printf("Syntax: <GPIO> <value>\n");
	std::vector<IOMap>::iterator it;
	for(it = ioMap.begin(); it != ioMap.end(); it++){
		printf("%s	->	%d\n", it->str, it->ioNum);
	}
	while(1){
		printf("> ");
		scanf("%d %d", &gpio_no, &gpio_val);
		if(is_gpio_valid(gpio_no)){
			if(gpio_val == 0){
				gpio_set_value(gpio_no, LOW);
				printf("GPIO %d set to LOW\n", gpio_no);
			}else{
				gpio_set_value(gpio_no, HIGH);
				printf("GPIO %d set to HIGH\n", gpio_no);
			}
		}else{
			printf("Invalid GPIO number!\n");
		}
	}
}

int main(int argc, char *argv[]){
	char *cstr = (char *)malloc(4096);
	int value;
	std::string strData;
	const char *value_str;
	std::vector<IOMap>::iterator it;
	initIoMap();
	initGPIO();
	if(argv[1] != NULL){
		testGPIO();
	}
	while(1){
		handle_url((char *)URL_STRING, cstr, 4096);
		strData = cstr;
		for(it = ioMap.begin(); it != ioMap.end(); ++it){
			value_str = strData.substr(strData.find(it->str,0) + strlen(it->str) + 2, 1).c_str();
			value = atoi(value_str);
			if(value){
				gpio_set_value(it->ioNum, HIGH);
			}else{
				gpio_set_value(it->ioNum, LOW);
			}
		}
	}
	return 0;
}



