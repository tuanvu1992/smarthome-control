/*
 * iomap.h
 *
 *  Created on: Jul 28, 2015
 *      Author: messier
 */

#ifndef IOMAP_H_
#define IOMAP_H_
#include <vector>
#define NUM_OF_DEVICES	27
typedef struct IOMap_t {
	char* str;
	unsigned int ioNum;
}IOMap;

IOMap iomap_array[NUM_OF_DEVICES] = {
		{(char*)"DENKHACH",			30},
		{(char*)"QUATKHACH",		60},
		{(char*)"TIVI",				31},
		{(char*)"CUACHINH",			50},
		{(char*)"DENTHANG",			48},
		{(char*)"DENBEP",			51},
		{(char*)"QUATBEP",			5},
		{(char*)"DENTAMC",			4},
		{(char*)"NNONGTAMC",		3},
		{(char*)"DENNGU1",			2},
		{(char*)"DENNGU2",			49},
		{(char*)"DENNGU3",			15},
		{(char*)"MAYLANH1",			14},
		{(char*)"MAYLANH2",			115},
		{(char*)"MAYLANH3",			112},
		{(char*)"QUATNGU1",			20},
		{(char*)"QUATNGU2",			47},
		{(char*)"QUATNGU3",			7},
		{(char*)"DENDOCSACH",		66},
		{(char*)"MAYLANHDOCSACH",	67},
		{(char*)"QUATDOCSACH",		69},
		{(char*)"DENTAM1",			68},
		{(char*)"DENTAM2",			45},
		{(char*)"DENTAM3",			44},
		{(char*)"MAYNNONG1	",		23},
		{(char*)"MAYNNONG2",		26},
		{(char*)"MAYNNONG3",		27}
};
std::vector<IOMap> ioMap;


#endif /* IOMAP_H_ */
