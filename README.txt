Smart home control using Beaglebone Black
Revision: R01A
- Run build.sh in DT Overlay folder to compile device tree overlay
- Copy file .dtbo to /lib/firmware
- Add this line to /etc/rc.local to load device tree at boot
echo IHearTech_SMART > /sys/devices/bone_capemgr.9/slots
- To run test program, pass any argument to SmartHome_Control when execute binary
